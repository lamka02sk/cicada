use chrono::{DateTime, Local};
use strum_macros::EnumString;
use strum_macros::Display;

#[derive(Display, Debug, PartialEq, EnumString)]
pub enum RecordCategory {

    #[strum(serialize="uncategorized")]
    Uncategorized,

    #[strum(serialize="success")]
    Success,

    #[strum(serialize="info")]
    Info,

    #[strum(serialize="warning")]
    Warning,

    #[strum(serialize="error")]
    Error

}

impl Default for RecordCategory {

    fn default() -> Self {
        Self::Uncategorized
    }

}

#[derive(Debug)]
pub struct Record {
    pub category: RecordCategory,
    pub message: String,
    pub datetime: DateTime<Local>
}

impl Default for Record {

    fn default() -> Self {
        Self {
            category: RecordCategory::default(),
            message: String::default(),
            datetime: Local::now()
        }
    }

}

impl Record {

    pub fn new(category: RecordCategory, message: String, datetime: Option<DateTime<Local>>) -> Self {
        Self {
            category, message, datetime: match datetime {
                Some(datetime) => datetime,
                None => Local::now()
            }
        }
    }

}

#[cfg(test)]
mod record_test {

    use super::*;

    #[test]
    fn can_be_instantiated() {

        let datetime = Local::now();
        let message = String::from("Test message");

        let instantiated = Record::new(RecordCategory::Info, message.clone(), Some(datetime.clone()));
        let manual = Record {
            category: RecordCategory::Info,
            message: message.clone(),
            datetime: datetime.clone()
        };

        assert_eq!(instantiated.category, manual.category);
        assert_eq!(instantiated.message, manual.message);
        assert_eq!(instantiated.datetime, manual.datetime);

    }

}