use std::sync::{Mutex, Arc};
use crate::structs::{Record, RecordCategory};
use chrono::{Local, DateTime};

#[derive(Default, Debug)]
pub struct Stats {
    pub logs: Vec<Record>,
    pub counter: Counter
}

impl Stats {

    pub fn new() -> Arc<Mutex<Self>> {
        Arc::new(Mutex::new(Self::default()))
    }

    pub fn log(&mut self, category: RecordCategory, message: &str, datetime: Option<DateTime<Local>>) {
        self.logs.push(Record::new(category, message.to_string(), datetime));
    }

}

#[derive(Default, Debug)]
pub struct Counter {
    pub total: u32,
    pub deployments: u32,
    pub failed: u32,
    pub stopped: u32
}

#[cfg(test)]
mod stats_tests {

    use super::*;

    fn new_stats() -> Stats {
        Stats::default()
    }

    #[test]
    fn push_into_logs() {

        let mut stats = new_stats();
        stats.log(RecordCategory::Success, "Logging works!!!", None);

        assert_eq!(stats.logs.len(), 1);

        let message = stats.logs.pop().unwrap();

        assert_eq!(message.category, RecordCategory::Success);
        assert_eq!(message.message, "Logging works!!!");
        assert_ne!(message.datetime, Local::now());

    }

    #[test]
    fn push_into_logs_with_datetime() {

        let mut stats = new_stats();
        let datetime = Local::now();

        stats.log(RecordCategory::Success, "Logging with datetime works!!!", Some(datetime.clone()));

        let message = stats.logs.pop().unwrap();
        assert_eq!(message.datetime, datetime.clone());

    }

}