use std::sync::{Mutex, Arc};
use crate::structs::Stats;
use crate::configuration::Configs;

#[derive(Debug, Clone)]
pub struct AppState {
    pub stats: Arc<Mutex<Stats>>,
    pub configs: Arc<Mutex<Configs>>
}

impl AppState {

    pub fn new() -> Self {
        
        AppState {
            stats: Stats::new(),
            configs: Configs::load()
        }
    
    }

}