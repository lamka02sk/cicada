pub mod system;
pub use system::*;

use std::sync::{Arc, Mutex};
use crate::helpers::filesystem::read_json_file;
use std::path::Path;

#[derive(Clone, Debug)]
pub struct Configs {
    pub system: System
}

impl Configs {

    pub fn load() -> Arc<Mutex<Self>> {

        Arc::new(Mutex::new(Self {
            system: Configs::load_system_config()
        }))

    }

    fn load_system_config() -> System {
        match read_json_file::<System>(&Path::new("config/system.json")) {
            Ok(config) => config,
            Err(error) => panic!("Could not load configuration file 'config/system.json': {}", error)
        }
    }

}

#[cfg(test)]
mod configs_test {

    use super::*;

    #[test]
    fn test_config_loader() {

        let configs = Configs::load();
        let mutex_lock = configs.lock();

        assert!(mutex_lock.is_ok());

    }

}