use serde::{Serialize, Deserialize};
use strum_macros::EnumString;
use strum_macros::Display;

#[derive(Clone, Debug, Default, Deserialize, Serialize)]
pub struct System {
    pub app_name: String,
    pub environment: Environment,
    pub host: String,
    pub path: String,
    pub port: u16,
    pub allow_origin: Vec<String>
}

impl System {

    pub fn get_bind(&self) -> String {
        self.host.clone() + ":" + &self.port.to_string()
    }

}

#[derive(Display, Clone, Debug, Deserialize, Serialize, PartialEq, EnumString)]
#[serde(rename_all = "lowercase")]
pub enum Environment {

    #[strum(serialize="0", serialize="production", to_string="production")]
    Production = 0,

    #[strum(serialize="1", serialize="development", to_string="development")]
    Development = 1

}

impl Default for Environment {

    fn default() -> Self {
        Environment::Production
    }

}

#[cfg(test)]
mod test_system_config {

    use super::*;

    #[test]
    fn generate_bind_address() {

        let system_config = System {
            host: String::from("example.com"), port: 7777,
            ..System::default()
        };

        assert_eq!(&system_config.get_bind(), "example.com:7777");

    }

}