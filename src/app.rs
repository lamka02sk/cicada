use actix_web::{App, HttpServer};
use crate::{state::AppState, services};
use actix_web::web::scope;

#[actix_web::main]
pub async fn start() -> std::io::Result<()> {

    let app_state = AppState::new();

    let configs = app_state.configs.clone();
    let system_config = &configs.lock().unwrap().system;

    let path = system_config.path.clone();

    HttpServer::new(move || {
        App::new()
            .data(app_state.clone())
            .service(scope(&path.clone()).configure(services::configure))
    })
        .server_hostname(&system_config.host)
        .bind(system_config.get_bind())?
        .run()
        .await

}