use std::path::Path;
use std::fs::{OpenOptions, File};
use std::io::{Read, BufReader};
use std::io::Result as IOResult;
use std::error::Error;
use serde::de::DeserializeOwned;

pub fn read_file(filename: &Path) -> IOResult<String> {

    let handle = OpenOptions::new().read(true).write(false).open(filename);

    if handle.is_err() {
        return Err(handle.err().unwrap());
    }

    let mut buffer= String::new();
    handle.unwrap().read_to_string(&mut buffer)?;

    Ok(buffer)

}

pub fn read_json_file<T>(filename: &Path) -> Result<T, Box<dyn Error>>
    where T: DeserializeOwned {

    let file = File::open(filename);

    if let Err(error) = file {
        return Result::Err(Box::new(error));
    }

    let reader = BufReader::new(file.unwrap());

    match serde_json::from_reader(reader) {
        Ok(result) => Result::Ok(result),
        Err(error) => Result::Err(Box::new(error))
    }

}

#[cfg(test)]
mod filesystem_test {

    use super::*;

    #[test]
    fn read_test_file() {
        assert!(read_file(Path::new("test/text_file.txt")).is_ok())
    }

    #[test]
    fn read_test_invalid_json_file() {

        use serde::Deserialize;

        #[derive(Deserialize)]
        struct TestJson {
            lorem: String,
            ipsum: i32,
            amet: Vec<String>
        }

        assert!(read_json_file::<TestJson>(Path::new("test/json_file.json")).is_err());

    }

    #[test]
    fn read_test_valid_json_file() {

        use serde::Deserialize;

        #[derive(Deserialize)]
        struct TestJson {
            lorem: String,
            dolor: i32,
            amet: Vec<String>
        }

        assert!(match read_json_file::<TestJson>(Path::new("test/json_file.json")) {
            Ok(_) => true,
            Err(error) => {
                println!("{}", error);
                false
            }
        });

    }

}