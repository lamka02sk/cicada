mod services;
mod state;
mod structs;
mod app;
mod helpers;
mod configuration;

fn main() -> std::io::Result<()> {
    app::start()
}